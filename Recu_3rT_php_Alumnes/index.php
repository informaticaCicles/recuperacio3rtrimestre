<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Examen Extraordinari PHP</title>
		<link  rel="stylesheet" href="./css/estils.css" />			
	</head>
	<body>
		<div id="wrapper">
			<header id="cap">
				<?php					
					include './include/cap.php';					
				?>
			</header>
			<section id="contingut">
				<?php					
					include './include/contingut.php';					
				?>
			</section>
			<footer id="peu">
				<?php					
					include './include/peu.php';					
				?>
			</footer>
		</div>
	</body>
</html>
