<div id="exercicis">
	
	<div id="exercici02">
		<h2>Exercici 02</h2>
		<p>
		 <span class="destacat">Enunciat:</span><br/>
		 Inserir en un camp una ip i detectar si és incorrecta.<br/>
		 És incorrecta si no té el format de 4 valors numèrics separats per punts.<br/>
		 És incorrecta si no és un valor entre 0.0.0.0 i 255.255.255.255.<br/>
		 Si és correcta, s'ha de detectar si és privada i de quina classe és.<br>
		 Classe A privada 10.X.X.X<br/>
		 Classe B privada 172.16.X.X a 172.31.X.X<br/>
		 Classe C privada 192.168.X.X<br/>
		 En cas de no ser privada, només ha de mostrar la ip i indicar que és correcta<br/>
		 En cas de ser privada, ha de redirigir cap a les pàgina classe.php, mostrant la ip i la imatge corresponent<br/>
		 
	 </p>
	 <form id="form_exercici02" method="POST" action="include/processaExercici02.php">
			<div class="filaform">
		    <span>IP</span>
		    <span><input type="text" name="ip" id="ip" required /></span>
		  </div>
			<div class="filaform">
				<span><input id="submitform" name="submitform" type="submit" value="Envia"/></span>
			</div>
   	</form>
	</div> <!--final exercici 02-->
	
</div>
