<?php
	//Codi alumne	
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Exercici 02 : IP correcta</title>
		<link  rel="stylesheet" href="../css/estilsProcessa.css" />			
	</head>
	<body>
		<div id="wrapper">
			<header id="cap">
				<img src="../img/logophp.png" alt="logo PHP" class="fotocap" />
				<h1>Examen extraordinari PHP :: Exercici 02</h1>
				<img src="../img/logophp.png" alt="logo PHP" class="fotocap" />
			</header>
			<section id="contingut">
			
			<!-- Resultat exercici 02 -->
				<article class="exercici">
					<?php
						// codi alumne
					?>
				</article>			
			
			</section>
			<footer id="peu">
				<?php					
					include 'peu.php';					
				?>
			</footer>
		</div>
	</body>
</html>
