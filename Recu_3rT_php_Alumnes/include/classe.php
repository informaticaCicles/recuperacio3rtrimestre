<?php
	//Codi alumne
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Exercici 02 : Classe <?php echo $classe; ?></title>
		<link  rel="stylesheet" href="../css/estilsProcessa.css" />
		<link  rel="stylesheet" href="../css/estilsHuit.css" />				
	</head>
	<body>
		<div id="wrapper">
			<header id="cap">
				<img src="../img/logophp.png" alt="logo PHP" class="fotocap" />
				<h1>Examen Extraordinari PHP :: Exercici 02</h1>
				<img src="../img/logophp.png" alt="logo PHP" class="fotocap" />
			</header>
			<section id="contingut">
			
			<!-- Resultat exercici 02 -->
				<article class="exercici">
					<?php
						//Codi alumne					
					?>
				</article>			
			
			</section>
			<footer id="peu">
				<?php					
					include 'peu.php';					
				?>
			</footer>
		</div>
	</body>
</html>
